#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define true 1
#define false 0

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int16_t i16;

#ifdef __cplusplus
}
#endif